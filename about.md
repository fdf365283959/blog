---
layout: page 
title: "关于我"
description: "保持学习，保持热爱"
header-img: "img/twitter.jpg"
---

![](https://i.ibb.co/KXdvpg8/20230129150003.png)

## 人生如梦亦如幻，朝如晨露暮如霞

> 欢迎来到 fandf 的网络世界。  
我是一个对生活充满热爱和憧憬的 IT 人，喜欢爬山(并不专业)，旅行；没事也会看看书，跑跑步。

### 联系我

> email: bluequin@163.com  
> 微信: fdf365283959